from django.db import models
from slugify import slugify


class News(models.Model):
    """
    Модель новости
    """
    title = models.CharField(
        max_length=100,
        help_text='Введите заголовок',
        verbose_name='Заголовок'
    )
    date = models.DateField(
        help_text='Введите дату новости',
        verbose_name='Дата'
    )
    slug = models.SlugField(
        max_length=452,
        unique=True,
        db_index=True,
        verbose_name='Слаг',
        help_text='Транслитерация названия, формируется автоматически при сохранении'
    )
    text = models.TextField(
        help_text='Введите текст новости',
        verbose_name='Текст',
    )

    class Meta:
        verbose_name = 'Новость'
        verbose_name_plural = 'Новости'
        db_table = 'News'

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.slug = slugify(str(self.title))
        super().save(*args, **kwargs)


class Tag(models.Model):
    """
    Модель тега новости
    """
    name = models.CharField(
        max_length=100,
        help_text='Введите название',
        verbose_name='Название',
    )
    news = models.ManyToManyField(
        News,
        related_name='tags',
        through='NewsTags',
    )

    class Meta:
        verbose_name = 'Тег'
        verbose_name_plural = 'Теги'
        db_table = 'Tags'

    def __str__(self):
        return self.name


class NewsTags(models.Model):
    """
    Модель связи новости с тегом
    """
    news = models.ForeignKey(
        News,
        on_delete=models.CASCADE,
    )
    tag = models.ForeignKey(
        Tag,
        on_delete=models.CASCADE,
        help_text='Выберите тег',
        verbose_name='Название тега',
    )

    class Meta:
        verbose_name = 'Тег'
        verbose_name_plural = 'Теги'
        db_table = 'NewsTags'

    def __str__(self):
        return self.tag.name


class Photo(models.Model):
    """
    Модель фотографии новости
    """
    image = models.ImageField(
        upload_to='news_images',
        help_text='Выберите фото',
        verbose_name='Фото',
    )
    news_id = models.ForeignKey(
        News,
        related_name='photos',
        on_delete=models.CASCADE,
    )
    is_main = models.BooleanField(
        default=False,
        help_text='Отметьте, чтобы фото стало главным фото новости',
        verbose_name='Является главным фото',
    )

    class Meta:
        verbose_name = 'Фотография'
        verbose_name_plural = 'Фотографии'
        db_table = 'Photos'

    def __str__(self):
        return self.image.name

    def save(self, *args, **kwargs):
        if self.is_main:
            Photo.objects.filter(
                news_id=self.news_id
            ).exclude(
                pk=self.pk
            ).update(is_main=False)
        super().save(*args, **kwargs)
