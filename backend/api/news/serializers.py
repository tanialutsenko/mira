from rest_framework.serializers import ModelSerializer

from api.news.models import News, Photo


class NewsPhotoSerializer(ModelSerializer):
    class Meta:
        model = Photo
        fields = [
            'id',
            'image',
            'is_main',
        ]


class NewsSerializer(ModelSerializer):
    photos = NewsPhotoSerializer(many=True)

    class Meta:
        model = News
        fields = [
            'id',
            'title',
            'date',
            'slug',
            'text',
            'photos',
        ]
        lookup_field = 'slug'
