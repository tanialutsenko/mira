from drf_spectacular.utils import (
    extend_schema,
    extend_schema_view,
    OpenApiParameter
)
from rest_framework import viewsets

from api.news.models import News
from api.news.serializers import NewsSerializer


@extend_schema_view(
    list=extend_schema(
        description='Просмотр всех новостей с пагинацией '
                    'и возможностью фильтра по тегу',
        parameters=[OpenApiParameter(
            name='tag',
            required=False,
            description='Filter by tag',
            type=str,
        )]
    ),
    retrieve=extend_schema(
        description='Просмотр новости по слагу',
    )
)
class NewsViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Класс для просмотра всех новостей
    """
    queryset = News.objects.all()
    serializer_class = NewsSerializer
    lookup_field = 'slug'

    def get_queryset(self):
        queryset = News.objects.all()
        tag = self.request.query_params.get('tag')
        if tag:
            queryset = queryset.filter(tags__name=tag)
        return queryset
