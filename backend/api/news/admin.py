from django.contrib import admin

from .models import News, Tag, Photo, NewsTags


class TagInline(admin.TabularInline):
    model = NewsTags
    extra = 0
    autocomplete_fields = ['tag']


class PhotoInline(admin.TabularInline):
    model = Photo
    extra = 0


class NewsAdmin(admin.ModelAdmin):
    fields = [
        'title',
        'date',
        'text',
        'slug',
    ]
    list_display = [
        'title',
        'date',
    ]
    readonly_fields = ('slug',)
    inlines = [PhotoInline, TagInline]
    exclude = ['tags']


class TagAdmin(admin.ModelAdmin):
    fields = ['name']
    search_fields = ['name']


class PhotoAdmin(admin.ModelAdmin):
    pass


admin.site.register(News, NewsAdmin)
admin.site.register(Tag, TagAdmin)
admin.site.register(Photo, PhotoAdmin)
