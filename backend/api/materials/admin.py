from django.contrib import admin

from .models import Material, BookType, Book, Law


class MaterialAdmin(admin.ModelAdmin):
    fields = [
        'title',
        'file',
    ]


class LawAdmin(admin.ModelAdmin):
    fields = [
        'title',
        'text',
    ]


class BookTypeAdmin(admin.ModelAdmin):
    fields = [
        'name',
    ]


class BookAdmin(admin.ModelAdmin):
    fields = [
        'author',
        'title',
    ]


admin.site.register(Material, MaterialAdmin)
admin.site.register(Law, LawAdmin)
admin.site.register(BookType, BookTypeAdmin)
admin.site.register(Book, BookAdmin)
