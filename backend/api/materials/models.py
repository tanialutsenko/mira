from django.db import models


class Law(models.Model):
    title = models.CharField(
        max_length=100,
        help_text='Введите заголовок',
        verbose_name='Заголовок'
    )
    text = models.TextField(
        help_text='Введите текст закона',
        verbose_name='Текст',
    )


class Material(models.Model):
    title = models.CharField(
        max_length=100,
        help_text='Введите заголовок',
        verbose_name='Заголовок'
    )

    file = models.FileField(
        upload_to='materials/materials_files/',
        max_length=100,

    )


class BookType(models.Model):
    name = models.CharField(
        max_length=100,
        help_text='Введите имя категории',
        verbose_name='Категория',
        unique=True
    )


class Book(models.Model):
    author = models.CharField(
        max_length=100,
        help_text='Введите имя автора',
        verbose_name='Имя автора'

    )
    title = models.CharField(
        max_length=100,
        help_text='Введите заголовок',
        verbose_name='Заголовок'
    )

    info = models.TextField(
        help_text='Введите описание книги',
        verbose_name='Описание',
    )
    book_type_id = models.ForeignKey(to=BookType, on_delete=models.PROTECT)
