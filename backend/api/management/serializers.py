from rest_framework.serializers import ModelSerializer

from api.management.models import Document, Team, Report


class TeamSerializer(ModelSerializer):
    class Meta:
        model = Team
        fields = '__all__'


class DocumentSerializer(ModelSerializer):
    class Meta:
        model = Document
        fields = '__all__'


class ReportSerializer(ModelSerializer):
    class Meta:
        model = Report
        fields = '__all__'
