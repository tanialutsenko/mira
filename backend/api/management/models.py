from django.db import models


# Create your models here.
class Team(models.Model):
    """
   Модель команды
   """
    name = models.CharField(
        max_length=50,
        help_text='Введите имя',
        verbose_name='Имя'
    )
    image = models.ImageField(
        help_text='Введите фотографию',
        verbose_name='Фотография',
        upload_to='team/images/'

    )
    position = models.CharField(
        max_length=50,
        help_text='Введите позицию сотрудника',
        verbose_name='Позиция'
    )
    info = models.TextField(
        help_text='Введите информацию о сотруднике',
        verbose_name='Информация'
    )

    class Meta:
        verbose_name = 'Команда'
        verbose_name_plural = 'Командa'
        db_table = 'Team'

    def __str__(self):
        return self.name


class Document(models.Model):
    """
   Модель документы
   """
    title = models.CharField(
        max_length=50,
        help_text='Введите  название документа',
        verbose_name='Название'
    )
    file = models.FileField(
        upload_to='document/img',
        help_text='Загрузите документ',
        verbose_name='Документ'
    )

    class Meta:
        verbose_name = 'Документ'
        verbose_name_plural = 'Документы'
        db_table = 'Document'

    def __str__(self):
        return self.title


class Report(models.Model):
    """
   Модель отчета
   """
    title = models.CharField(
        max_length=50,
        help_text='Введите  название отчета',
        verbose_name='Название отчета'
    )
    file = models.FileField(
        upload_to='documents/',
        help_text='Загрузите отчет',
        verbose_name='Отчет'
    )
    date = models.DateField(
        help_text='Введите дату отчета',
        verbose_name='Дата'
    )

    class Meta:
        verbose_name = 'Отчет'
        verbose_name_plural = 'Отчеты'
        db_table = 'Report'

    def __str__(self):
        return self.title
