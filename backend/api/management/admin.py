from django.contrib import admin
from .models import Team, Document,Report
# Register your models here.

class ManagementAdmin(admin.ModelAdmin):
    fields = [
        'name',
        'image',
        'position',
        'info',
    ]
    list_display = [
        'name',
        'position',
    ]


class DocumentAdmin(admin.ModelAdmin):
    fields = [
        'title',
        'file',
    ]
    list_display = [
        'title',
    ]


class ReportAdmin(admin.ModelAdmin):
    fields = [
        'title',
        'file',
        'date',
    ]
    list_display = [
        'title',
        'date',
    ]


admin.site.register(Team, ManagementAdmin)
admin.site.register(Document, DocumentAdmin)
admin.site.register(Report, ReportAdmin)
