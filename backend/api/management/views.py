from django.shortcuts import render
from drf_spectacular.utils import (
    extend_schema,
    extend_schema_view,
    OpenApiParameter
)
from rest_framework import viewsets

from api.management.models import Team, Document, Report
from api.management.serializers import TeamSerializer, DocumentSerializer, ReportSerializer


class TeamViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Класс для просмотра команды
    """
    queryset = Team.objects.all()
    serializer_class = TeamSerializer


class DocumentViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Класс для просмотра всех документов
    """
    queryset = Document.objects.all()
    serializer_class = DocumentSerializer


class ReportViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Класс для просмотра всех отчетов
    """
    queryset = Report.objects.all()
    serializer_class = ReportSerializer
