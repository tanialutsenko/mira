from django.contrib import admin
from .models import ProjectType, Project, Paragraph


class ParagraphInline(admin.TabularInline):
    model = Paragraph


@admin.register(ProjectType)
class ProjectTypeAdmin(admin.ModelAdmin):
    fields = (
        'name',
        'description',
        'icon',
        'slug',
    )
    list_display = (
        'name',
        'description',
    )
    readonly_fields = (
        'slug',
    )
    search_fields = (
        'name',
        'description',
    )


@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    fields = (
        'title',
        'image',
        'description',
        'type_id',
    )
    list_display = (
        'title',
        'description',
    )
    search_fields = (
        'title',
        'description',
    )
    inlines = [
        ParagraphInline,
    ]


@admin.register(Paragraph)
class ParagraphAdmin(admin.ModelAdmin):
    fields = (
        'image',
        'text',
        'index',
    )
    list_display = (
        'text',
        'index',
    )
    search_fields = (
        'text',
        'index',
    )
