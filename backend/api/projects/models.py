from django.db import models
from slugify import slugify


class ProjectType(models.Model):
    """
    Модель типов проектов
    """
    name = models.CharField(
        max_length=1024,
        help_text='Введите название типа проекта',
        verbose_name='Название типа проекта',
    )
    description = models.TextField(
        help_text='Введите описание типа проекта',
        verbose_name='Описание типа проекта'
    )
    icon = models.ImageField(
        upload_to='project_types/icons/',
        help_text='Выберите иконку типа проекта',
        verbose_name='Иконка типа проекта',
        blank=True,
    )
    slug = models.SlugField(
        max_length=255, unique=True,
        db_index=True,
        verbose_name='Слаг',
        help_text='Транслитерация типа проекта, формируется автоматически при сохранении',
    )

    class Meta:
        verbose_name = 'Тип проекта'
        verbose_name_plural = 'Типы проектов'
        db_table = 'ProjectType'

    def __str__(self) -> str:
        return f'Тип проекта "{self.name}"'

    def save(self, *args, **kwargs) -> None:
        self.slug = slugify(str(self.name))
        return super().save(*args, **kwargs)


class Project(models.Model):
    """
    Модель проекта
    """
    title = models.CharField(
        max_length=1024,
        help_text='Введите заголовок проекта',
        verbose_name='Заголовок проекта',
    )
    image = models.ImageField(
        upload_to='projects/images/',
        help_text='Выберите изображение проекта',
        verbose_name='Изображение проекта',
    )
    description = models.TextField(
        help_text='Введите описание проекта',
        verbose_name='Описание проекта',
    )
    type_id = models.ForeignKey(
        to=ProjectType, on_delete=models.CASCADE,
        help_text='Выберите ID типа проекта',
        verbose_name='ID типа проекта',
    )

    class Meta:
        verbose_name = 'Проект'
        verbose_name_plural = 'Проекты'
        db_table = 'Project'

    def __str__(self) -> str:
        return f'Проект "{self.title}"'


class Paragraph(models.Model):
    """
    Модель параграфа
    """
    image = models.ImageField(
        upload_to='paragraphs/images/',
        help_text='Изображение параграфа',
        verbose_name='Изображение параграфа',
    )
    text = models.TextField(
        help_text='Введите текст параграфа',
        verbose_name='Текст параграфа',
    )
    index = models.IntegerField(
        help_text='Введите индекс параграфа',
        verbose_name='Индекс параграфа',
    )
    project = models.ForeignKey(
        to=Project, on_delete=models.CASCADE,
        help_text='Выберите ID проекта',
        verbose_name='ID проекта',
    )

    class Meta:
        verbose_name = 'Параграф'
        verbose_name_plural = 'Параграфы'
        db_table = 'Paragraph'

    def __str__(self) -> str:
        return f'Параграф "{self.text}"'
