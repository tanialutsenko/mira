from django.apps import AppConfig


class CommunicationConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'api.communication'
    verbose_name = 'Отзывы и Партнёры'
