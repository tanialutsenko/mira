from django.contrib import admin

from .models import Partners, Feedback


class FeedbackAdmin(admin.ModelAdmin):
    fields = [
        'author',
        'text',
        'image',
    ]
    list_display = [
        'author',
        'text',
        'image',
    ]


class PartnersAdmin(admin.ModelAdmin):
    fields = [
        'logo',
    ]
    list_display = [
        'logo',
    ]


admin.site.register(Feedback, FeedbackAdmin)
admin.site.register(Partners, PartnersAdmin)
