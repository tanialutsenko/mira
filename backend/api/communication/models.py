from django.db import models


class Feedback(models.Model):
    """
    Модель отзыва
    """
    author = models.CharField(
        max_length=100,
        help_text='Введите автора отзыва',
        verbose_name='Автор отзыва'
    )
    text = models.TextField(
        help_text='Введите текст отзыва',
        verbose_name='Текст отзыва',
    )
    image = models.ImageField(
        upload_to='static/feedback/icons/',
        help_text='Выберите картинку автора',
        verbose_name='Картинка автора отзыва',
        blank=True,
    )

    class Meta:
        verbose_name = 'Отзыв'
        verbose_name_plural = 'Отзывы'
        db_table = 'feedback'

    def __str__(self):
        return self.author


class Partners(models.Model):
    """
    Модель партнёра
    """
    logo = models.ImageField(
        upload_to='partners/logos/',
        help_text='Выберите логотип партнёра',
        verbose_name='Логотип партнёра',
        blank=True,
    )

    class Meta:
        verbose_name = 'Партнёр'
        verbose_name_plural = 'Партнёры'
        db_table = 'partners'

    def __str__(self):
        return self.id
